# API Automation Framework for OMDB API
### Requirement:
- Java 11
- Maven

### Tech:
- Java 11
- Maven
- Serenity

### Setup
1. Clone projects
```
  > git clone 
```
2. For running tests
```
  > mvn clean verify
```
3. For getting Serenity report 
```
  localy - ./target/site/serenity/index.html
  within CI/CD - https://gitlab.com/evgeniy.otsevich/omdb-api-tests/-/pipelines/ and download artifacts
```

### Project Structure
```
Java:
    Api:
        Base:
            ApiClientController - Parent API Controller class
        OMDB: 
            OmdbApiController - API Controller for OMDB service
            OmdbStepDefinitions - Steps definitions for OMDB features files
Resources:            
    Features:
        OMDB:
            (list of festures files) - Test cases on BDD format
```

### Developing tests:

    1. Create .feature file in /Resources/Features/OMDB
    2. Update API controller accordingly 
    3. Implement Step Definitions

### Covered Endpoints
```
    http://www.omdbapi.com/?apikey=[yourkey]&
        with params:
            - t -> By ID or Title -> Movie title to search for.
            - s -> By Search -> Movie title to search for.
            
        NB! For additionali information please visit: https://www.omdbapi.com/    
```
