package api.base;

import io.restassured.builder.RequestSpecBuilder;
import io.restassured.filter.log.RequestLoggingFilter;
import io.restassured.filter.log.ResponseLoggingFilter;
import io.restassured.specification.RequestSpecification;
import net.thucydides.core.util.EnvironmentVariables;
import net.thucydides.core.util.SystemEnvironmentVariables;

public class ApiClientController {

    protected EnvironmentVariables env = SystemEnvironmentVariables.createEnvironmentVariables();

    protected final RequestSpecification baseClientSpec = new RequestSpecBuilder()
            .addFilter(new ResponseLoggingFilter())
            .addFilter(new RequestLoggingFilter())
            .build();

}
