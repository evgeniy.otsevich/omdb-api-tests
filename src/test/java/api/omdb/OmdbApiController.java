package api.omdb;

import api.base.ApiClientController;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.specification.RequestSpecification;
import net.serenitybdd.core.environment.EnvironmentSpecificConfiguration;
import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.annotations.Step;

public class OmdbApiController extends ApiClientController {

    private final RequestSpecification restSpec = new RequestSpecBuilder()
            .setBaseUri(EnvironmentSpecificConfiguration.from(env).getProperty("base.url"))
            .addQueryParam("apikey",EnvironmentSpecificConfiguration.from(env).getProperty("api.key"))
            .build();

    @Step("Get movies by title {0}")
    public void getMoviesByTitle(String titleValue) {
        SerenityRest.given()
                .spec(baseClientSpec)
                .spec(restSpec)
                .queryParam("t", titleValue)
                .get();
    }

    @Step("Get movies by  title {0} and type {0}")
    public void getMoviesByTitleAndType(String titleValue, String movieType) {
        SerenityRest.given()
                .spec(baseClientSpec)
                .spec(restSpec)
                .queryParam("s", titleValue)
                .queryParam("type", movieType)
                .get();
    }
}
