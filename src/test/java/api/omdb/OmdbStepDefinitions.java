package api.omdb;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.annotations.Steps;

import static org.hamcrest.Matchers.*;

public class OmdbStepDefinitions {

    @Steps
    OmdbApiController apiController;

    @When("I look up a movie by {} title")
    public void i_look_up_a_movie_by_title(String titleValue) {
        apiController.getMoviesByTitle(titleValue);
    }

    @When("I look up a movie by {} and non-valid {} type")
    public void i_look_up_a_movie_by_tile_and_type(String titleValue, String movieType) {
        apiController.getMoviesByTitleAndType(titleValue, movieType);
    }

    @Then("Response status should be {int}")
    public void response_status_should_be(Integer statusCode) {
        SerenityRest.restAssuredThat(response -> response.statusCode(statusCode));
    }

    @Then("Resulting movie should have {} in title")
    public void resulting_movies_should_be(String titleValue) {
        SerenityRest.restAssuredThat(response -> response.body("Title", containsString(titleValue)));
    }

    @Then("Response should contains error {}")
    public void response_should_contains_error(String error) {
        SerenityRest.restAssuredThat(response -> response.body("Error", equalTo(error)));
    }
}
