Feature: Search movie by title and non valid type

  Scenario Outline: Search movies by title and non-valid type
    When I look up a movie by <Title> and non-valid <Type> type
    Then Response should contains error Movie not found!
    Examples:
      | Title | Type   |
      | King | Carton |
      | Lion | anime |



