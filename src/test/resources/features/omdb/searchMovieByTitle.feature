Feature: Search movie by title

  Scenario Outline: Search movies by title
    When I look up a movie by <Title> title
    Then Response status should be 200
    And Resulting movie should have <Title> in title
    Examples:
      | Title   |
      | Comedy |
      | King   |
      | Few    |



